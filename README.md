# DS COURSE TEMPLATE - INTRODUCTION
This repository is a template for Data Society Courses. It includes the ability to automate the generation of course assets in order to make it a more efficient and consistent process. 

* The [CLIENT] - [COURSE NAME] section is the standard template for Course Repos and should be filled out.
* The ARTIFACT PIPELINE AUTOMATION section is a user manual for the automation features. 
* This INTRODUCTION section may be deleted.

---

# [CLIENT] - [COURSE NAME]

## Key facts & people
- **Date created:** [...]
- **Language(s):** [...]
- **Software used:** 
	- [...]
- **Author:** [Name, Email]
- **DS manager:** [Name, Email]
- **ID manager:** [Name, Email]
- **Contributers:** [Names]

---

### What is this repository for?
[...]

---

### Description ###

[...]

---

### Tags ###
[...]

---

### How do I get set up?
- [...]

---

### Reference trainings / modules ###

**Training name:** [...]

**Reference repo(s):** [...]

**Reference module(s):** [...]

---

# ARTIFACT PIPELINE AUTOMATION USER MANUAL

For instructions on how to use the artifact pipeline automation for scraping please refere to the following documents:

* [For the Data Science team](https://bitbucket.org/datasociety/course-template/src/main/user_manuals/Data%20Science%20User%20Manual.md)
* For the Instructional Design team: Yet to be documented
* [For testing the pipeline locally](https://bitbucket.org/datasociety/course-template/src/main/user_manuals/Local%20Setup%20and%20Testing.md)
