
# Generic - IntroToClustering

## Key facts & people
- **Date created:** : 09 September 2021
- **Programming Language(s):** Python
- **Software requirements:**  Anaconda, Python, Jupyter Notebooks
- **Author:** Nupur Neti, nupur@datasociety.com
- **DS manager:** Nupur Neti, nupur@datasociety.com
- **ID manager:** Meghan Cipperley, meghan@datasociety.com
- **Contributers:** 
- **Version:** 1.0

---

### Description ###

This course covers the unsupervised learning method called clustering which is used to find patterns or groups in data without the need for labelled data. This course includes different methods of clustering both numerical and categorical data including centroid-based, density-based, distribution-based and hierarchical-based clustering and how to build, evaluate and interpret these models. 

### Tags ###
clustering, k-means, k-modes, mean-shift, spherical, dbscan, k-prototypes

### Prerequisites ###
IntroToPython, IntroToDataVisualization

### Target Audience ###
This is an introductory level course for data scientists who want to learn to detect and visualize underlying patterns and groups in unlabelled data and how to handle different types of data.

### Delivery Platform ###
Jupyter Hub

### Customization Options ###
Jupyter Hub, Gitlab, Google Drive, Box 


