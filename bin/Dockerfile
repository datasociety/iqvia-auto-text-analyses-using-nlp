# Based on official miniconda image built on lightweight linux image
# Includes Python and Pip
FROM continuumio/miniconda3:4.10.3-alpine as base

# add linux and shell packages
RUN apk update
RUN apk upgrade
RUN apk add bash
RUN apk add git

# Install node and npm
RUN apk add --update nodejs npm
RUN apk add --update npm

# Install R language
RUN apk add R

# upgrade pip and python tools
RUN pip3 install --upgrade pip setuptools wheel

# install aws cli
RUN pip3 install awscli

COPY . .

FROM base AS aws-config

# define arguments to be provided while building the image
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_DEFAULT_REGION
ARG AWS_CA_REPO
ARG AWS_CA_DOMAIN
ARG YAML_FILE
ADD $YAML_FILE ./

# create aws credentials file and config file
RUN aws configure --profile default set aws_access_key_id $AWS_ACCESS_KEY_ID && \
    aws configure --profile default set aws_secret_access_key $AWS_SECRET_ACCESS_KEY && \
    aws configure --profile default set region AWS_DEFAULT_REGION

# configure pypi client to access CodeArtifact repo
RUN aws codeartifact login --tool pip --repository $AWS_CA_REPO --domain $AWS_CA_DOMAIN

FROM aws-config AS conda-env

# Create and activate conda environment, install custom packages
RUN NAME=$(grep name $YAML_FILE | cut -c7-); \
    conda env create -f $YAML_FILE; \
    source /opt/conda/etc/profile.d/conda.sh; \
    conda activate $NAME; \
    echo "source /opt/conda/etc/profile.d/conda.sh" >> ~/.bash_profile; \
    echo "conda activate $NAME" >> ~/.bash_profile; \
    echo "source /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc; \
    echo "conda activate $NAME" >> ~/.bashrc; \
    python -m nltk.downloader -d /usr/local/share/nltk_data all

# TODO: see if we can only download nltk dump if nltk is present in the conda.yaml file.

ENTRYPOINT ["bash", "-l", "-c"]