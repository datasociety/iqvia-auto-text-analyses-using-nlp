---
title: AIML 302 - Part 10
subtitle: "One should look for what is and not what he thinks should be. (Albert Einstein)"
output:
 revealjs::revealjs_presentation:
  css: !expr here::here("dependencies/slides_rmd.css")
  transition: slide
  template: !expr here::here("dependencies/reveal_template.html")
  reveal_options:
   width: 1366
   height: 768
---

```{r, echo=FALSE, include=FALSE, eval=TRUE}
library(reticulate)
library(here)
library(knitr)
library(stringr)
library(jsonlite)
library(dplyr)
library(ggplot2)

session_info = sessionInfo()
print(session_info)

platform = session_info$platform
main_dir = here()
conda_yaml = readLines(con = paste0(main_dir, "/bin/conda.yml"))
env_name = strsplit(conda_yaml[1], split='name: ')[[1]][2]
use_condaenv(env_name)
data_dir = paste0(main_dir, "/data")
plot_dir = paste0(main_dir, "/plots")
```


```{python, echo=FALSE}
main_dir = r.main_dir
data_dir = r.data_dir
plot_dir = r.plot_dir
```


Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
    <tr>
    <td>Explain the concept of logistic regression and how it will be used in this case</td>
    <td></td>
  </tr>
 <tr>
  <td>Initialize, build, train the logistic regression model on our training dataset and predict on test</td>
  <td></td>
  </tr>
  <tr>
    <td>Summarize classification performance metrics</td>
    <td></td>
 </tr>
</table>

Directory settings
=================================================
- Encode your directory structure into `variables`
- Let the `data_dir` be the variable corresponding to your `data` folder

```{python include_chunk1, eval=FALSE}
data_dir = "/home/jovyan/iqvia-aiml-302/data"
plot_dir = "/home/jovyan/iqvia-aiml-302/plots"
```



Loading packages
=================================================
```{python}
# Helper packages.
import os
import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
```

```{python}
# Packages with tools for text processing.
import nltk
```

```{python, echo=FALSE, results='hide'}
import nltk
nltk.download('all')
```

```{python}
# Packages for working with text data and analyzing sentiment
from nltk.sentiment.vader import SentimentIntensityAnalyzer 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
```

```{python}
# Packages to build and measure the performance of a logistic regression model 
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import preprocessing
import warnings
warnings.filterwarnings('ignore')
```


Import data we saved 
=================================================
- We have already created these pre-saved pickles
- Let's now import them to continue with sentiment analysis today!

```{python}
# Load pickled data and models.
score_labels = pickle.load(open(data_dir + "/score_labels.sav","rb"))
DTM_matrix = pickle.load(open(data_dir + '/DTM_matrix.sav',"rb"))
DTM_array = DTM_matrix.toarray()
```

Model building - split the dataset
=================================================
:::::: {.columns}
::: {.column width="60%"}
- We will use `train_test_split` from `scikit-learn` to split our dataset
<br>

```{python}
X_train, X_test, y_train, y_test  = train_test_split(
        DTM_array, 
        score_labels,
        train_size = 0.70, 
        random_state = 1234)
```

:::
::: {.column width="40%"}
```{python}
print(len(X_train))
print(len(X_test))
print(len(y_train))
print(len(y_test))
```


:::
::::::

Fun quiz
=================================================
~~Answer in chat:~~ Is logistic regression a supervised machine learning algorithm?

1. YES
2. NO

<div class="notes">
Use these to feel out how much the class knows about logistic and how fast or slow to go through the slides

Answer: YES
</div>

Fun quiz
=================================================
~~Answer in chat:~~ Suppose you have been given a fair coin and you want to find out the ~~probability~~ and the **odds** of landing on heads. Which of the following pairs of options is true for such a case? 

1. ~~0~~ and **0.5** 
2. ~~0.5~~ and **2** 
3. ~~0.5~~ and **1** 
4. None of these

<div class="notes">
Answer: 0.5 and 1
</div>

Fun quiz
=================================================
~~Answer in chat:~~ The sigmoid function plays a key role in logistic regression

1. TRUE
2. FALSE


<div class="notes">
Answer: TRUE
</div>


Fun quiz
=================================================
:::::: {.columns}
::: {.column width="50%"}
~~Answer in chat:~~ The figure shows ROC curves for three logistic regression models. Different colors show curves for different hyperparameter values. Which of the following ROC will give best result?

1. Yellow
2. Pink
3. Black
4. All are same

:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/quiz-roc.png")`)


<div class="notes">
Answer: YELLOW
</div>

:::
::::::
Model building - logistic regression
=================================================
:::::: {.columns}
::: {.column width="50%"}
- Let's briefly review the concept of logistic regression 
  - **Supervised** machine learning method
  - Target/dependent variable is <b>binary</b> (one/zero)
  - Outputs the **probability** that an observation will be in the desired class (`y = 1`)
  - Solves for coefficients to create a <i>curved</i> function to maximize the likelihood of        correct classification
  - `logistic` comes from the `logit` function (*a.k.a. inverse sigmoid function*) 

:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/logistic.png")`)

:::
::::::
Logistic regression: when to use it?
=================================================
- Since `logistic` regression is a ~~supervised machine learning~~ algorithm, we will use it to:

  - ~~Classify~~ data into categories

- Since `logistic` regression outputs **probabilities** and not actual class labels, it can be used to:
  - Easily tweak its performance by adjusting a **cut-off probability** instead of re-running the model with new parameters  

- Since `logistic` regression is a <b class="purple-emphasis">well-established algorithm</b> with multitudes of implementations across many programming languages, it can be used to:

  - Create <b class="purple-emphasis">robust</b>, <b class="purple-emphasis">efficient</b> and <b class="purple-emphasis">well-optimized models</b>


Logistic regression: process
=================================================
![centered](`r here::here("dependencies/img/logisticsteps.png")`)

Categorical to binary target variable
=================================================
:::::: {.columns}
::: {.column width="50%"}
- Preparing the target variable: translate an existing binary variable (i.e. any categorical variable with 2 classes) into `1` and `0` 
  
  - we'll convert our labels to a binary variable
  - `scikit-learn` has a package `preprocessing` that can do this for you
  - `LabelBinarizer` will transform a list of strings into 1/0
  - it also works for multi-class problems - for complete documentation, click [here](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelBinarizer.html)
  
:::
::: {.column width="50%"}

![centered-border](`r here::here("dependencies/img/label_binazer.png")`)

:::
::::::
Categorical to binary target variable
=================================================
- Let's convert our labels in our `y_test` list
```{python}
# Initiate the Label Binarizer.
lb = preprocessing.LabelBinarizer()

# Convert y_test to binary integer format. 
y_test= lb.fit_transform(y_test)
```

![centered](`r here::here("dependencies/img/categoricalconvert.png")`)

Logistic regression: function
=================================================
- For every value of $x$, we find $p$, i.e. probability of success, or probability that $y = 1$ 
- To solve for $p$, logistic regression uses an expression called a <b class="purple-emphasis">sigmoid function:</b>
<br>

$$p = \frac{exp(ax + b)}{1 + exp(ax + b)}$$

<br>

- Although looking pretty involved and scary (nobody likes exponents!), we can see a very ~~familiar equation inside of the parentheses:~~ $ax + b$


<div class = "notes">
- When you **substitute** $p$ **with the expression above into the equation of a logit function** from the previous slide and perform some algebraic magic, **you will get a simple equation of the line**

$$\hat{y} = logit(p) = log \big(\frac{p}{1-p}\big) = ax + b$$

<p class="centered-text"><i class="orange-emphasis">The link function <code>logit</code> transforms an S-shaped sigmoid function into a straight line. That is why logistic regression is considered a part of GLM family of models!</i></p>
</div>

Logistic regression: a bit more math 
=================================================

Through some algebraic transformations that are beyond the scope of this course, 

<br>
$$p = \frac{exp(ax + b)}{1 + exp(ax + b)}$$ 
can become 

<br>
$$logit(p) = log \big(\frac{p}{1-p}\big)$$

- Since `p` is the <b class="dark-green-emphasis">probability of success</b>, `1 - p` is the <b class="maroon-emphasis">probability of failure</b>
- The ratio $\big(\frac{p}{1-p}\big)$ is called the ~~odds~~ ratio, it simply tells us the ~~odds~~ of having a successful outcome with respect to the opposite
- <b> Why should we care?</b> Knowing this provides useful insight into interpreting the coefficients


Logistic regression: coefficients
=================================================
<p class="centered-text"> $ax + b$ </p>

- In **linear** regression, the coefficients can easily be interpreted
- Increase in $x$ will result in an increase in $y$ and vice versa

<b>BUT</b>

- In ~~logistic~~ regression, the simplest way to interpret a positive coefficient is with an increase in likelihood 
- Larger $x$ increases the likelihood of $y = 1$


<div class="notes">
- One of the main assumptions for `logisic` regression is the distribution of the errors in the model (and the outcome variable $Y$ itself) that can take on only 2 values `0` or `1`; that distribution must be `binomial`
- Another assumption is having the <b class="purple-emphasis">linear relationship between the transformed outcome variable and explanatory variables</b>; that transformation function is `logit` and it <b class="purple-emphasis">links</b> predicted and predictor variables!
</div>

Logistic regression: connection to neural nets 
=================================================

- The curved _sigmoid_ function is a widely used function not only in logistic regression

<br>

$$p(y = 1) = \frac{exp(ax + b)}{1 + exp(ax + b)}$$ 

<br>

- Due to the properties and range of probabilities it produces, the sigmoid function is often used as an ~~activation function~~ in **neural networks** algorithms




Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
    <tr>
    <td>Explain the concept of logistic regression and how it will be used in this case</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
 <tr>
  <td>Initialize, build, train the logistic regression model on our training dataset and predict on test</td>
  <td></td>
  </tr>
  <tr>
    <td>Summarize classification performance metrics</td>
    <td></td>
 </tr>
</table>

scikit-learn - logistic regression
=================================================
- We will be using the `LogisticRegression` library from `scikit-learn.linear_model` package

![centered-border](`r here::here("dependencies/img/logistic-regression-scikit.png")`)

- All inputs are optional arguments
- Right now, we will build a base model
- For all the parameters of the `LogisticRegression` function, visit [scikit-learn's documentation](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html)

Logistic regression: build
=================================================
- Let's build our logistic regression model, we will use all default parameters for now as our baseline model

```{python}
# Set up logistic regression model.
log_model = LogisticRegression()
print(log_model)
```

Logistic regression: fit
=================================================
:::::: {.columns}
::: {.column width="50%"}
The two main arguments are : 

1. `X_train`: a `pandas` dataframe or a `numpy` array of train data predictors
2. `y_train`: a `pandas` series or an a `numpy` array of train labels 

<br>

```{python}
# Fit the model.
log_model = log_model.fit(X = X_train, y = y_train)
```

:::
::: {.column width="50%"}

![centered-border](`r here::here("dependencies/img/logistic-fit.png")`)

:::
::::::
Logistic regression: predict
=================================================
:::::: {.columns}
::: {.column width="50%"}
The main argument is : 

1. `X_test`: a `pandas` dataframe or a `numpy` array of test data predictors

:::
::: {.column width="50%"}

![centered-border](`r here::here("dependencies/img/logistic-predict.png")`)


:::
::::::
Logistic regression: predict (cont'd)
=================================================
```{python}
# Predict on test data.
y_pred = log_model.predict(X_test)
print(y_pred)
```

```{python}
# Convert y_pred to binary integer format. 
y_pred= lb.fit_transform(y_pred)
```



Knowledge check 1
=================================================
![centered](`r here::here("dependencies/img/knowledge_check.png")`)

Exercise 1
=================================================
![centered](`r here::here("dependencies/img/exercise.png")`)


Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
    <tr>
    <td>Explain the concept of logistic regression and how it will be used in this case</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
 <tr>
  <td>Initialize, build, train the logistic regression model on our training dataset and predict on test</td>
  <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
  <tr>
    <td>Summarize classification performance metrics</td>
    <td></td>
 </tr>
</table>

Model building - analyze results
=================================================
- **We now have**:
  
  - model that predicts sentiment
  - predictions from the model for the test dataset

- <b>We want</b>:

  - accuracy of our model
  - methods to tune and optimize our model
  
*To review the performance of our model, we look at:*

 - Confusion matrix
 - ROC curve
 - AUC

Confusion matrix
=================================================
<table>
  <tr>
    <th></th>
    <th>Low yield</th>
    <th>High yield</th>
    <th>Predicted totals</th>
  </tr>
  <tr>
    <td>Predicted low yield</td>
    <td><b class="green-emphasis">True positive (TP)</b></td>
    <td><b class="maroon-emphasis">False positive (FP)</b></td>
    <td>Total predicted positive</td>
  </tr>
  <tr>
    <td>Predicted high yield</td>
    <td><b class="maroon-emphasis">False negative (FN)</b></td>
    <td><b class="green-emphasis">True negative (TN)</b></td>
    <td>Total predicted negative</td>
  </tr>
  <tr>
    <td><b>Actual totals</b></td>
    <td>Total positives</td>
    <td>Total negatives</td>
    <td><b>Total</b></td>
  </tr>
</table>

<br>

- <b class="green-emphasis">True positive rate (TPR)</b> (a.k.a *Sensitivity*, *Recall*) = <b class="green-emphasis">TP</b> / Total positives
- <b class="green-emphasis">True negative rate (TNR)</b> (a.k.a *Specificity*) = <b class="green-emphasis">TN</b> / Total negatives
- <b class="maroon-emphasis">False positive rate (FPR)</b> (a.k.a *Fall-out*, *Type I Error*) = <b class="maroon-emphasis">FP</b> / Total negatives
- <b class="maroon-emphasis">False negative rate (FNR)</b> (a.k.a *Type II Error*) = <b class="maroon-emphasis">FN</b> / Total positives
- <b class="green-emphasis">Accuracy</b> = <b class="green-emphasis">TP + TN</b> / <b>Total</b>
- <b class="maroon-emphasis">Misclassification rate</b> = <b class="maroon-emphasis">FP + FN</b> / <b>Total</b>

From threshold to metrics
=================================================
- In logistic regression, the output is a range of probabilities from `0` to `1`
- But how do you interpret that as a `1`/`0` or `High yield`/`Low yield` label? 
- You set a ~~threshold~~ where everything above is predicted as `1` and everything below is predicted `0`
- A typical threshold for logistic regression is `0.5`

![centered](`r here::here("dependencies/img/logitconfus.png")`)


From metrics to a point
=================================================
Each threshold can create a confusion matrix, which can be used to calculate a point in space defined by:

- <b class="green-emphasis">True positive rate (TPR)</b> on the `y-axis`
- <b class="maroon-emphasis">False positive rate (FPR)</b> on the `x-axis`

![centered](`r here::here("dependencies/img/plotthreshold.png")`)

From points to a curve
=================================================
![centered](`r here::here("dependencies/img/roccreation.png")`)

- When we move thresholds, we re-calculate our metrics and create confusion matrices for every threshold
- Every time, we plot a new point in the <b class="green-emphasis">TPR</b> vs <b class="maroon-emphasis">FPR</b> space

ROC: receiver operator characteristic 
================================================
:::::: {.columns}
::: {.column width="45%"}

- The result of plotting points in <b class="green-emphasis">TPR</b> vs <b class="maroon-emphasis">FPR</b> space is a curve called **R**eceiver **O**perator **C**haracteristic (**ROC**)
- It shows a trade-off between the two rates
- It is a common and one of the best ways to assess performance of classification models

:::
::: {.column width="55%"}

![centered](`r here::here("dependencies/img/ROC_ex.png")`)

:::
::::::
AUC: area under the curve
================================================
:::::: {.columns}
::: {.column width="45%"}

- It is a **performance metric** used to compare classification models to measure ~~predictive accuracy~~
- The <b>AUC</b> should be <b>above .5</b> to say the model is better than a random guess
- The perfect <b>AUC</b> `= 1` (you will never see this number working with real world data!)

:::
::: {.column width="55%"}

![centered](`r here::here("dependencies/img/AUC_explained.png")`)

:::
::::::
scikit-learn: metrics package
================================================
![centered-border](`r here::here("dependencies/img/metrics-scikit.png")`)

- We will use the following methods from this library:

  - `confusion_matrix`
  - `accuracy_score`
  - `classification_report`
  - `roc_curve`
  - `auc`

- For all the methods and parameters of the `metrics` package, visit [scikit-learn's documentation](http://scikit-learn.org/stable/modules/classes.html#sklearn-metrics-metrics)

Confusion matrix and accuracy
=================================================
Both `confusion_matrix` and `accuracy_score` take 2 arguments:

1. Original data labels 
2. Predicted labels 

```{python}
# Take a look at test data confusion matrix.
conf_matrix_test = metrics.confusion_matrix(y_test, y_pred)
print(conf_matrix_test)
```

```{python}
# Compute test model accuracy score.
test_accuracy_score = metrics.accuracy_score(y_test, y_pred)
print("Accuracy on test data: ", test_accuracy_score)
```

Classification report
=================================================
- To make interpretation of `classification_report` easier, in addition to the 2 arguments that `confusion_matrix` takes, we can add the actual class names for our target variable

```{python}
# Create a list of target names to interpret class assignments.
target_names = ['Negative', 'Positive']
```

```{python}
# Print an entire classification report.
class_report = metrics.classification_report(y_test, 
                                             y_pred, 
                                             target_names = target_names)
print(class_report)
```

Classification report (cont'd)
=================================================
```{python}
print(class_report)
```

- `precision` is <b class="green-emphasis">Positive Predictive Value</b> = <b class="green-emphasis">TP</b> / (<b class="green-emphasis">TP</b> + <b class="maroon-emphasis">FP</b>)
- `recall` is <b class="green-emphasis">TPR</b> = <b class="green-emphasis">TP</b> / Total positives
- `f1-score` is a weighted harmonic mean of `precision` and `recall`, where it reaches its best value at `1` and worst score at `0`
- `support` is actual number of occurrences of each class in `y_test`

Getting probabilities instead of class labels
=================================================
```{python}
# Get probabilities instead of predicted values.
test_probabilities = log_model.predict_proba(X_test)
print(test_probabilities[0:5, :])
```

```{python}
# Get probabilities of test predictions only.
test_predictions = test_probabilities[: , 1]
print(test_probabilities[0:5])
```

Computing FPR, TPR and threshold
=================================================
```{python}
# Get FPR, TPR and threshold values.
fpr, tpr, threshold = metrics.roc_curve(y_test,           #<- test data labels
                                        test_predictions) #<- predicted probabilities
print("False positive: ", fpr)
print("True positive: ", tpr)
print("Threshold: ", threshold)
```


Computing AUC
=================================================
```{python}
# Get AUC value
auc = metrics.roc_auc_score(y_test,y_pred)
print("Area under the ROC curve: ", auc)
```

Putting it all together: ROC plot
=================================================
:::::: {.columns}
::: {.column width="55%"}

```{python, echo=FALSE}
plt.rcParams.update({'font.size': 15})
```
- Our model has an accuracy of about `0.68` - it's good, but might be based on a biased target
- We also have an AUC of about `0.56`, which is not all that great
- You may want to look at these aspects to improve the model:

  - balancing the target variable and methods to do so
  - using the TF-IDF matrix for a more weighted and normalized base

:::
::: {.column width="45%"}
```{python, eval=FALSE}
plt.title('Receiver Operator Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.show()
```
```{python, fig.width=8, fig.height=8, fig.align = "center", echo=FALSE}
# Make an ROC curve plot.
_=plt.title('Receiver Operator Characteristic')
_=plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % auc)
_=plt.legend(loc = 'lower right')
_=plt.plot([0, 1], [0, 1],'r--')
_=plt.xlabel('False Positive Rate')
_=plt.ylabel('True Positive Rate')
plt.show()
```

:::
::::::
Knowledge check 2
=================================================
![centered](`r here::here("dependencies/img/knowledge_check.png")`)

Exercise 2
=================================================
![centered](`r here::here("dependencies/img/exercise.png")`)


Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
    <tr>
    <td>Explain the concept of logistic regression and how it will be used in this case</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
 <tr>
  <td>Initialize, build, train the logistic regression model on our training dataset and predict on test</td>
  <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
  <tr>
    <td>Summarize classification performance metrics</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
 </tr>
</table>



What next?
=================================================
- Throughout this course, we have learned how to deal with text data, visualized text data and extracted meaningful insights out of it using unsupervised learning methods like LDA
- In the near future, you might want to explore more about advanced topics like ~~classification and neural networks~~ that are part of the supervised learning domain
  - Supervised machine learning is the task of predicting or classifying an output based on predictors, or input variables
  - Below are some examples of how you would apply these algorithms in business scenarios

<table>
  <tr>
    <th>Question to answer</th>
    <th>Real world example</th>
  </tr>
  <tr>
    <td>What is the value based on predictors?</td>
    <td>Predicting the number of bikes rented based on the weather</td>
  </tr> 
  <tr>
    <td>What category is this in?</td>
    <td>Anticipating if your customer is pregnant, remodeling, just got married, etc.</td>
  </tr>
  <tr>
    <td>What is the probability that something is in a given category?</td>
    <td>Determining the probability that a piece of equipment will fail, or that someone will buy your product</td>
  </tr>
</table>


This completes our module
=================================================
![icon-left-bottom](`r here::here("dependencies/img/circles-crayon-purple.png")`)
