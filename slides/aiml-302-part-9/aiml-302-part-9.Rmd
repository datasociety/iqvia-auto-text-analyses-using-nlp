---
title: AIML 302 - Part 9
subtitle: "One should look for what is and not what he thinks should be. (Albert Einstein)"
output:
 revealjs::revealjs_presentation:
  css: !expr here::here("dependencies/slides_rmd.css")
  transition: slide
  template: !expr here::here("dependencies/reveal_template.html")
  reveal_options:
   width: 1366
   height: 768
---

```{r, echo=FALSE, include=FALSE, eval=TRUE}
library(reticulate)
library(here)
library(knitr)
library(stringr)
library(jsonlite)
library(dplyr)
library(ggplot2)

session_info = sessionInfo()
print(session_info)

platform = session_info$platform
main_dir = here()
conda_yaml = readLines(con = paste0(main_dir, "/bin/conda.yml"))
env_name = strsplit(conda_yaml[1], split='name: ')[[1]][2]
use_condaenv(env_name)
data_dir = paste0(main_dir, "/data")
```


```{python, echo=FALSE}
main_dir = r.main_dir
data_dir = r.data_dir
```

Warm up
=================================================
- Take a few minutes to craft a “tweet” of about 140 characters that either:

  - poses a question about something we've discussed
  - sums up something valuable you've learned
  - offers a resource you've discovered on your own
  - asks for additional information

- When you're done, post your tweet in the chat so we can pick out a few to discuss in more
detail

Welcome back!
=================================================
- In the last class we learned about:

  - Feature engineering and word embeddings
  - Creating a Gensim Word2vec model to view similar words
  - Using pre-trained word embeddings to generate text features for model development
  - Computing cosine similarity and finding similar documents

- Today we will talk about sentiment analysis and logistic regression.

Are the Humans "Happy?"
=================================================
:::::: {.columns}
::: {.column width="50%"}
  - Can algorithms pick up on human "feelings"?
  - When we are in the realm of text analysis, we can get close with ~~sentiment analysis~~
  
:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/emojifeel.png")`)

:::
::::::
Sentiment in customer support
=================================================
:::::: {.columns}
::: {.column width="50%"}
- Working with ~~customer support data~~

  - Analyzing positive and negative sentiment, trying to improve customer service

- Working with ~~chat messages~~

  - Analyzing positive and negative sentiment, trying to detect and improve the biggest triggers for unhappy customers

  
:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/chat-messages.png")`)

  
:::
::::::
Sentiment in product development
=================================================
:::::: {.columns}
::: {.column width="50%"}
- Working with **consumer reviews** 

  - Analyzing positive and negative sentiment, trying to improve product offerings
  
- Working with **movie reviews** 

  - Analyzing positive and negative sentiment, trying to improve movie selection offered with subscription service
  
  
:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/movie.png")`)

:::
::::::
Large Text/Historical uses
=================================================
:::::: {.columns}
::: {.column width="50%"}
  - After one of the largest releases of email in corporate history due to legal action, thousands of Enron communications were made available for text analysis.
  - Hundreds of new studies sprouted up pursuing questions as diverse as social network theory, community and anomaly detection, gender and communication within organizations, behavioral change during an organizational crisis, and insularity and community formation. 
  
:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/enron.png")`)

You can read about that work [here](https://programminghistorian.org/en/lessons/sentiment-analysis)

:::
::::::
Sentiment analysis - use cases
=================================================
:::::: {.columns}
::: {.column width="50%"}
- Sentiment mining is used in many areas:
  
  - opinion mining
  - reputation monitoring
  - business analytics

- It helps businesses understand their customers' experience
- **How do you see sentiment analysis as a helpful tool within your job/field?**

:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/sentiment.png")`)


:::
::::::
Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
  <tr>
    <td>Summarize the concept of sentiment analysis</td>
    <td></td>
  </tr>
  <tr>
    <td>Classify each cleaned sentence from the cleaned text file as positive, negative, or neutral and store labels</td>
    <td></td>
  </tr>
  <tr>
    <td>Split the data into train and test set for classification</td>
    <td></td>
  </tr>
</table>

Directory settings
=================================================
- Encode your directory structure into `variables`
- Let the `data_dir` be the variable corresponding to your `data` folder

```{python include_chunk1, eval=FALSE}
data_dir = "/home/jovyan/iqvia-aiml-302/data"
```


Loading packages
=================================================
```{python}
# Helper packages.
import os
import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
```

```{python}
# Packages with tools for text processing.
import nltk
```

```{python, echo=FALSE, results='hide'}
import nltk
nltk.download('all')
```

```{python include_chunk4b, eval = FALSE}
# Packages for working with text data and analyzing sentiment
from nltk.sentiment.vader import SentimentIntensityAnalyzer 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
```

```{python, echo = FALSE, include = FALSE}
# Packages for working with text data and analyzing sentiment
from nltk.sentiment.vader import SentimentIntensityAnalyzer 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
```

```{python, echo = FALSE, include = FALSE}
# Packages to build and measure the performance of a logistic regression model 
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import preprocessing
```

```{python include_chunk4c, eval = FALSE}
# Packages to build and measure the performance of a logistic regression model 
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import preprocessing
import warnings
warnings.filterwarnings('ignore')
```


"Bag-of-words" to sentiment analysis
=================================================
- We learned about the **bag-of-words** method
- We are now going to ~~use the Document Term Matrix (DTM) we built earlier to classify the sentiment of each newspaper snippet~~


Refresher: what is a DTM?
=================================================
:::::: {.columns}
::: {.column width="50%"}
![centered](`r here::here("dependencies/img/DTM.png")`)

:::
::: {.column width="50%"}

- ~~Document-term matrix~~ is simply a matrix of unique words counted in each document:

  - Documents are arranged in the rows
  - Unique terms are arranged in columns
- The corpus **vocabulary** consists of all of the unique terms (i.e. column names of DTM) and their total counts across all documents (i.e. column sums)

<div class="notes">
A Term-Document matrix will be just the transpose of the Document-Term matrix, with terms in rows and documents in columns 
</div>


<div class="notes">
Have the students discuss ways that they can use bag-of-words in their workstreams
</div>

:::
::::::


Sentiment analysis - steps 
=================================================
:::::: {.columns}
::: {.column width="50%"}
- There are ~~many ways to build out sentiment analysis~~, including what we will use today
- Some of the processes will use more complex methodology / algorithms, but **today, we want to understand the basis of sentiment analysis**
- <b>Using these base steps, you will be able to build upon it</b> on your own as you continue using sentiment analysis


:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/sentiment-analysis-chart.png")`)

:::
::::::
Sentiment analysis in Python 
=================================================
:::::: {.columns}
::: {.column width="50%"}
- **Python is very powerful when it comes to text mining, as you have seen**
- We will split the process we will be using into two parts for simplicity - text classification and model building
- ~~We will perform text classification today using `NLTK` and assign the lablels to the text sentences in our dataset~~

:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/sentiment_analysis.png")`)

:::
::::::
Sentiment analysis in Python 
=================================================
:::::: {.columns}
::: {.column width="50%"}
<b>Text classification</b>

  - **Classify** articles using NLTK's `SentimentIntensityAnalyzer`
  - **Load the DTM** that we have saved as a `pickle` (if we did not have one, we would build a DTM at this step)

:::
::: {.column width="50%"}
<b>Model building</b>

  -  ~~Split the DTM into train and test datasets~~, including the sentiment labels for each article
  - ~~Build a logistic regression model~~ on the train dataset that classifies texts as one of the two categories, "negative" or "positive"
  - ~~Analyze results~~ by predicting on the test set and on new data
  - ~~Optimize the model~~ after inspecting results
  - ~~Update the model~~ when new data comes in to have a continuously updated sentiment model

:::
::::::
Knowledge check 1
=================================================
![centered](`r here::here("dependencies/img/knowledge_check.png")`)

Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
  <tr>
    <td>Summarize the concept of sentiment analysis</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
  <tr>
    <td>Classify each cleaned sentence from the cleaned text file as positive, negative, or neutral and store labels</td>
    <td></td>
  </tr>
  <tr>
    <td>Split the data into train and test set for classification</td>
    <td></td>
  </tr>
</table>

Text classification - classify snippets
=================================================
:::::: {.columns}
::: {.column width="50%"}
- Today, we are going to classify each of our newspaper snippets using the `SentimentIntensityAnalyzer` function from the `vader` package
- They will be classified by document and classified as either:

  - "negative"
  - "positive"

:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/neg_pos.png")`)
  
:::
::::::
Text classification - classify snippets
=================================================
- Let's load the snippets to see what we are working with

```{python}
cleaned_txt = pickle.load(open(data_dir + '/NYT_clean_list.sav',"rb"))
```

Text classification - classify snippets
=================================================
```{python}
print(cleaned_txt[0:10])
```

- You can see we have a list of each snippet as a string
- Each snippet has been cleaned and stemmed, ready for scoring

Text classification - introducing vader
=================================================
- As mentioned earlier, we will be using the `SentimentIntensityAnalyzer` function from the `vader` package, which is in the `NLTK` library
- We loaded this function earlier, let's take a quick look at what it does:

  - <b>VADER</b> = <b>V</b>alence <b>A</b>ware <b>D</b>ictionary for s<b>E</b>ntiment <b>R</b>easoning
  - `vader` sentiment analysis relies on a dictionary which maps lexical features to emotion intensities called *sentiment scores*
  - `vader.SentimentIntensityAnalyzer` will return a score in the range -1 to 1 from most negative to positive
  - The sentiment score is calculated by summing up the sentiment scores of each `vader` dictionary listed word in the sentence
  - `vader` works best on short documents, like this example, and tweets and other types of messages
  - You can go to the [nltk.sentiment.vader module documentation](https://www.nltk.org/api/nltk.sentiment.html) page for the source code

Text classification - classify
=================================================
- We will now use `vader.SentimentIntensityAnalyzer` to classify our loaded and cleaned snippets
- Within this function, we will be specifically using `SentimentIntensityAnalyzer.polarity_scores` 
- This will give us a score between -1 and 1 and then compound the negative, neutral, and positive valences to give us a combined score, which is the score we will use today

Text classification - classify (cont'd)
=================================================

![centered-border](`r here::here("dependencies/img/polarity_scores.png")`)

Text classification - classify (cont'd)
=================================================
- Let's take a look at what the function outputs when iterating through each cleaned sentence

```{python}
# Initialize the `SentimentIntensityAnalyzer().`
sid = SentimentIntensityAnalyzer()

# Iterate through each sentence printing out the scores for each.
for sentence in cleaned_txt[:3]:
     print(sentence)
     ss = sid.polarity_scores(sentence)
     for k in ss:
         print('{0}: {1}, '.format(k, ss[k]), end='')
     print()
```


Text classification - classify (cont'd)
=================================================
- We use `SentimentIntensityAnalyzer` to write a function that:
    
  - takes the cleaned text, runs the `SentimentIntensityAnalyzer` on each snippet
  - outputs results in four scores: <b>"neg","neu","pos","compound"</b>
  - takes the "compound" score and uses that to analyze if the snippet is negative or positive
  - Returns the labels for each snippet in a list



Text classification - classify (cont'd)
=================================================
:::::: {.columns}
::: {.column width="55%"}

```{python}
# This function outputs a list of labels for each snippet:
def sentiment_analysis(texts):
  list_of_scores = []
  for text in texts:
      sid = SentimentIntensityAnalyzer()               
      compound = sid.polarity_scores(text)["compound"] 
      if compound >= 0:
          list_of_scores.append("positive")
      else:
          list_of_scores.append("negative")
  return(list_of_scores)
```

:::
::: {.column width="45%"}
```{python}
score_labels = sentiment_analysis(cleaned_txt)
```


```{python}
print(score_labels[1:5])
```
- You now have a list `score_labels` that contains a sentiment classification for each snippet 
- These will be used as your `y` variable when you build your train and test datasets

<div class = "notes">
The students should run this code, let them know it will take a a little time to run.
Worst case you can also tell them they can load the pickled list (if it is taking too long to run)
</div>


:::
::::::
Text classification - Load the DTM
=================================================
- Let's load the `DTM` from the list of pre-saved pickles
- We will convert it to an array for easier usage and call the array `DTM_array`
```{python}
DTM_matrix = pickle.load(open(data_dir + '/DTM_matrix.sav',"rb"))
DTM_array = DTM_matrix.toarray()
# Let's look at the first few rows of the finalized array. 
print(DTM_array[1:4])
```

- We see a ~~sparse matrix with counts for each word in the sentences~~
- We still have the same number of rows, but we have a column for each word that appears within any of the sentences
- **Because we have a numeric representation, we can now model the text**


Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
  <tr>
    <td>Summarize the concept of sentiment analysis</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
  <tr>
    <td>Classify each cleaned sentence from the cleaned text file as positive, negative, or neutral and store labels</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
  <tr>
    <td>Split the data into train and test set for classification</td>
    <td></td>
  </tr>
</table>


Model building - split the dataset
=================================================
:::::: {.columns}
::: {.column width="50%"}
- We now start going into the model-building steps
- We will be using a well-known library, `scikit-learn`
- This library contains many well-known ~~machine learning~~ packages
- For documentation and more information, you can go to the [scikit-learn homepage](https://scikit-learn.org/stable/)

:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/scikitlearn.png")`){height=70%}

:::
::::::
Model building - split the dataset
=================================================
:::::: {.columns}
::: {.column width="50%"}
- Our next step is to get our dataset ready for the actual <b>model building</b> 
- Once you have cleaned data in numeric form, you are ready to ~~split your data into a train and test set~~
- You also need to make sure you have your **target variable**

  - In this case, the targets are the **sentiment labels we created, negative / positive** 

:::
::: {.column width="50%"}

![centered](`r here::here("dependencies/img/holdout.png")`)

:::
::::::
Model building - split the dataset
=================================================
:::::: {.columns}
::: {.column width="50%"}
~~Train~~

- This is the data that you ~~train your model on~~
- Usually about ~~70% of your dataset~~
- Use a larger portion of the data for training, so that the model gets a ~~large enough sample of the population~~


:::
::: {.column width="50%"}
**Test**

- This is the data that you **test your model on**
- Usually about **30% of your dataset**
- Use a smaller portion to test your trained model on

:::
::::::


Model building - split the dataset
=================================================
:::::: {.columns}
::: {.column width="60%"}
- We will use `train_test_split` from `scikit-learn` to split our dataset
<br>
<b>The inputs to the function are</b>:

  1. `DTM_array` : the sparse matrix with the word counts for each 'document' (newspaper snippets in our case)
  2. `score_labels` : the sentiment we calculated for each document, which is our target variable that we want to predict
  3. `train_size` : the size of the training dataset, here we choose .7 or 70% of the entire dataset
  4. `random_state`: this randomizes the split for you

:::
::: {.column width="40%"}
```{python, echo=FALSE}
import warnings
warnings.filterwarnings("ignore")
```

```{python}
X_train, X_test, y_train, y_test  = train_test_split(
        DTM_array, 
        score_labels,
        train_size = 0.70, 
        random_state = 1234)
```


:::
::::::
Model building - split the dataset
=================================================
:::::: {.columns}
::: {.column width="50%"}
<b>The four outputs from this function will be:</b>

  1. `X_train` : the sparse matrix split into a 70% training sample
  2. `X_test`  : the remaining 30% of the sample, the 'holdout' set to test the trained model on
  3. `y_train` : the corresponding labels to `y_train`
  4. `y_test`  : the corresponding labels to `y_test`

:::
::: {.column width="50%"}
- Make sure that these variables exist
```{python}
print(len(X_train))
print(len(X_test))
print(len(y_train))
print(len(y_test))
```


:::
::::::
Save results as a pickle
=================================================
- <b>Note:</b> This is applicable only when you have access to your file management system or local data directories

  - Nevertheless, it's a standard practice to save intermediate data to prevent re-running code
- Pickle all generated data for next steps


```{python}
pickle.dump(score_labels, open(data_dir + '/score_labels.sav', 'wb'))
```

Knowledge check 2
=================================================
![centered](`r here::here("dependencies/img/knowledge_check.png")`)

Exercise 1
=================================================
![centered](`r here::here("dependencies/img/exercise.png")`)


Module completion checklist
=================================================
<table>
  <tr>
    <th>Objective</th>
    <th>Complete</th>
  </tr>
  <tr>
    <td>Summarize the concept of sentiment analysis</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
  <tr>
    <td>Classify each cleaned sentence from the cleaned text file as positive, negative, or neutral and store labels</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
  <tr>
    <td>Split the data into train and test set for classification</td>
    <td><p class="centered-text"><b class="green-emphasis" style="font-size: 42px;">&#10004;</b></p></td>
  </tr>
</table>

Summary
=================================================
- So far, we have

  - learned the concept of sentiment analysis and its use cases
  - classified sentences and labeled them using `vader` package
  
- Next, we'll perform learn about the concept of logistic regression and use it to classify our text sentences based on the labels we generated today!

- Until then, stay excited!



This completes our module
=================================================
![icon-left-bottom](`r here::here("dependencies/img/circles-crayon-purple.png")`)
