
# ARTIFACT PIPELINE AUTOMATION USER MANUAL

## Overview

*   The setup process involves tasks to be completed at the initialization of a new course repository. The result is that an automated script will run every time there is an update pushed to the main branch of the repo.
*   This script looks in the `slides/` folder and then scrapes `.Rmd` files for code content. It saves each one as a separate file and converts to `HTML` and `.pdf` formats. Then it copies all files and folders from `slides/` to a specified Google Drive folder, which can be shared with course participants. 
*   It also puts the `data/` folder into the Google Drive folder.


## Setup Tasks for Repository

1. Fork from template repository
    *   When creating a new repo, fork this template repo. ([Instructions here](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/))
    *   This template includes a few critical resources: 
        *   `bitbucket-pipelines.yml` file that triggers the automation process every time there is a push to the main branch of the repo
        *   `conda.yaml` and `conda-local.yml` files that specifies the default dependency requirements for the repo. This can and should be updated and managed. See below for instructions on how to keep the dependcies updated.

2. Save with new name

3. Enable Pipelines:
    *    `Repository Settings > Pipelines > Settings > Enable Pipelines`
    
4. Create Google Drive folder for Final content
    *   This will be the folder shared with course participants, so it must be within the course google folder in the appropriate client folder.
    *   Confirm that `dev@datasociety.com` has edit access to the folder.
    
5. Copy folder id and save as repo environment variable DESTINATION
    *   Find the folder id (the long string at the end of the URL when you are browsing the folder on the web)
    *   Copy it
    *   In the repo, go to `Repository settings > Repository variables`
    *   Add a new repository variable called `DESTINATION` and use the google folder id as the value
6. Organize files in the bitbucket repository
    *   All files that will ultimately be uploaded to Google Drive need to live inside the `slides` folder
    *   This includes the Rmd files but also the manually created Knowledge Check pdfs and the exercise notebooks. Note: Although Knowledge Checks will continue to be edited in Google Drive, they should be downloaded and saved in the module folder once they pass final review. 
    *   Within that folder follow any folder structure that course requires, for example,:
        * slides
            * week1
                * day1
                    * week1day1module.Rmd
                    * week1day1-exercises.ipynb
                    * week1day1-exercises-with-answers.ipynb
                    * Knowledge_Checks_week1day1.pdf
                * day2
                    * week1day2module.Rmd
                    * ...
            * week2
                * day1
                    * week2day1module.Rmd
                    * ...
        
        The scraper will recreate any folder structure within `slides` so the naming of the folder can be made appropriate to the course needs. 

     * The image folder `img` should be within the `dependencies` folder NOT within the `slides` folder. Anything within the `slides` folder is now pushed to Google Drive so if the `img` folder is within `slides` it will be uploaded to Drive as well and we do not want that. 
     * Folders like `plots` and `widgets` can live within the `slides` folder or outside based on the course requirements.
     * File paths in .uploadignore file ignored, all other files uploaded to Google Drive. If you wish to explicitly ignore a file, add it to to the .uploadignore file of your course repo. 
     
7. Create and manage `conda.yml` and `conda-local.yml` file that specifies conda environment requirements
    *   The repo template comes with a default `conda.yml` file, which is to be updated and managed by the course's data science team. Please make sure to add versions. Specifically for R packages, refer to [this link] (https://docs.anaconda.com/anaconda/packages/r-language-pkg-docs/)
    *   The name of the env is `python-course-test` by default however please create a new environment for every course and work in that enviornment during development and update packages as needed. Follow similar steps as done [here](https://bitbucket.org/datasociety/cafe-v2/src/master/) to create a conda env and update the yml file. 
        Here is a [conda cheatsheet](https://docs.conda.io/projects/conda/en/latest/_downloads/843d9e0198f2a193a3484886fa28163c/conda-cheatsheet.pdf) for other conda tasks. 
    * `conda-local.yml` is an exact replica of `conda.yml` however it is missing packages     - `course-scraper` and `google-drive-upload` which get installed on the docker image for scraping but cannot be installed on local machines as of now
    * Use `conda-local.yml` to create the environment on all local machines and update it with any packages needed for rendering the slides, the same as you do for `conda.yml`
    
## Setup Tasks for Rmd files

We will be using Rmd files moving forward. A sample Rmd file `sample_slides.Rmd` is provided in this repo. Please note the changes in directory settings and the image references.

1. If reusing old Rpres files, convert `.Rpres` files to `.Rmd`
    *   You can use [this library (rpres-rmd-converter)](https://bitbucket.org/datasociety/rpres-rmd-converter) to convert them to  `.Rmd`.
    *   Clone down the repo and run it locally to install the package
    *   Redirect to the repo with the Rpres files and run the command `python -m rpres_rmd_converter` It will search for .Rpres files, convert them to .Rmd. It is recommended that .Rpres files be deleted once the conversion is complete.
    If we are starting a course from scratch then begin development in .Rmd itself. 


## Scraping

1. Once you are ready to scrape, push changes in repo to main branch to automatically run pipeline
2. Go to the google drive folder that you linked in the reposiutory variable `DESTINATION`. All `.Rmd` modules within the `slides` folder should now be scraped and uploaded to the folder as follows: 

    *   if code snippets detected as `R`:
        *   produces `_code.R`
        *   produces `_exercises_and_answers.R`
    *   if code snippets detected as SQL:
        *   produces `_code.sql`
        *   produces `_exercises_and_answers.sql`
    *   if code snippets detected as Python:
        *   produces `_code.py`, triggers notebook scraper
            1. produces `_code.ipynb`
    *   written in something else (ex: instructor notes)
        *   ignores 
2. `.Rmd` files converted to HTML
3. HTML files converted to PDF


## Additional Details

This automation makes primary use of two other scripting libraries. Additional information can be found in their repos:

*   [ds-course-scraping](https://bitbucket.org/datasociety/ds-course-scraping)
*   [google-drive-upload](https://bitbucket.org/datasociety/google-drive-upload)

The pipeline makes use of a customized dockerfile, which can be found here: [ds-docker-images](https://bitbucket.org/datasociety/ds-docker-images).

If you need to convert `.rpres` to `.rmd`, you can use the [vatslr rpres-rmd-converter](https://bitbucket.org/datasociety/rpres-rmd-converter).

