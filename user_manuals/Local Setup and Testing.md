### Instructions for macOS (x86_64)

1. Install gcloud by following instructions from here: https://cloud.google.com/sdk/docs/install
2. Initialize gcloud

        a. `gcloud init`
        b. Login with your datasociety google account
        c. Select the number associated with `engineeringsandbox` project
        d. Type `n`(No) for default Compute Region and Zone

3. Pull docker image from GCP Container Registry with the following command
`docker pull gcr.io/engineeringsandbox/course-template-local-testing`

### To generate .html files from .Rmd

1. Ensure that the current working directory contains the following folders:
`slides`, `dependencies`, `data` (optional), and other necessary folders for sucessfully generating .html files from .Rmd files
2. The command below mounts the current working directory into the docker container and converts .Rmd files to .html files.
For example, my current working directory is `/Users/vatsal/Documents/git-repos/course-template/`
3. Copy and paste this directory after `-v` option:
`docker run --rm -v <your current working directory containing all necessary folders>:/tmp gcr.io/engineeringsandbox/course-template-local-testing "cd /tmp; python3 -m course_scraper /tmp/slides/ ./"`
For me, the command looks like as shown below:
`docker run --rm -v /Users/vatsal/Documents/git-repos/course-template/:/tmp gcr.io/engineeringsandbox/course-template-local-testing "cd /tmp; python3 -m course_scraper /tmp/slides/ ./"`


### To generate .pdf files from .html files

1. Open a `bash` shell by typing `bash` in the current shell, because the following commands work only with `bash` or `sh` shell.
2. Ensure that you are in the same current working directory as before. Your current working directory should now contain a file named `html_files.txt` and a new folder named `assets`, both created by docker container
3. Download the following docker image with this command `docker pull astefanutti/decktape`
4. Execute this command `while read p; do echo "$p"; arr=($p); docker run --rm -t -v $PWD:/slides astefanutti/decktape --size=2048x1152 reveal ${arr[0]} ${arr[1]}; done <html_files.txt`